import argparse
import gameNetworking as gn
import sys
import time
import copy

X = 'X'
O = 'O'

# SERVER IS O
# CLIENT IS X

dirs = [(-1,0), (-1,1), 
        (0,1) , (1,1) ,
        (1,0) , (1,-1),
        (0,-1), (-1,-1) ]


class Board(list):

    def __init__(self, isServer):
        for x in range(3):
            self.append([' ', ' ', ' '])

        self.isServer = isServer
        self.turns = 0  
        self.winner = 0
        
        if (isServer):
            self.player = O
            self.opponent = X
        else:
            self.player = X
            self.opponent = O


    def runGame(self):

        while self.turns < 9 and self.winner == 0:
            self.turns += 1
            print "Turn :", self.turns
            
            if (self.isServer + self.turns) % 2 == 0: # WE NEED TO LISTEN

                print "Waiting for opponent."
                opponentMove = gn.startListener()


                time.sleep(0.1) #Give time for sender to setup

                player = self.opponent
                i = int(opponentMove)

                status = self.play(player, i)
                print "Opponent (",player,") just played",i
                self.printBoard()

                if status:
                    print "Invalid move, game ending."
                    return 1


            else:
                # WE NEED TO MAKE A MOVE AND SEND

                i = int( raw_input("Enter pos: ") )

                status = self.play(self.player , i)
                print "Me (",self.player,") just played",i
                self.printBoard()

                gn.startTalker(str(i) )
                
                if status:
                    print "Invalid move, game ending."
                    return 1



            self.winner = self.check()

        self.gameOverStatement()

        return 0


    def play(self, player, pos):
        """
        pos goes from 1 to 9
        """

        i = (pos-1) // 3
        j = (pos-1) %  3
        if self[i][j] != ' ':
            print "Position (",i,',',j,") is taken by", self[i][j]
            return 1

        self[i][j] = player
        return 0

    def check(self):
        """
        Check if anyone has won yet.
        0 - No one
        1 - X has won
        2 - O has won
        """
        for i in range(3):
            for j in range(3):
                for (dx,dy) in dirs:
                    s = ''
                    ti, tj = i, j
                    for _ in range(3):
                        if ti < 0 or ti >= 3 or tj < 0 or tj >= 3:
                            break
                        s += self[ti][tj]
                        ti += dy
                        tj += dx

                    if s == 'XXX':
                        return 1
                    if s == 'OOO':
                        return 2


        return 0

    def gameOverStatement(self):
        if self.winner == 0:
            print "It is a TIE!"
        else:
            winner = X
            if self.winner == 2:
                winner = 0

            print "Player", winner, "has won!"

            if self.player == winner:
                print "Congratulations!"
            else:
                print "Sorry :( maybe next time."

    def printBoard(self):
        tempo = copy.deepcopy(self)
        for i in range(3):
            for j in range(3):
                if tempo[i][j] == ' ':
                   tempo[i][j] = '-'


        for i in range(3):
            print " | ".join(tempo[i])
            if i < 2:
                print '-'*9
        print ''



parser = argparse.ArgumentParser()
parser.add_argument('--server', action='store_true')
parser.add_argument('--client', action='store_true')
args = parser.parse_args()

if args.server and args.client:
    print 'Too many flags, pass one only.'

elif not args.server and not args.client:
    print 'Pass a server/client flag.'

elif args.server:
    Board(1).runGame()

elif args.client:
    Board(0).runGame()



