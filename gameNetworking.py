import socket
import sys

HOST = ''
PORT = 11111

def startListener( verbose = 0):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #MAYBE IS A FIX
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 

    s.bind((HOST, PORT))
    s.listen(1)

    c, addr = s.accept()
    if verbose:
        print "Got connection from", addr
    
    msg = c.recv(1)

    c.shutdown(socket.SHUT_RDWR)
    c.close()

    s.shutdown(socket.SHUT_RDWR)
    s.close()

    return msg

def startTalker( msg):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    #MAYBE IS A FIX
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1) 

    s.connect((HOST, PORT))

    s.sendall(msg)
    s.shutdown(socket.SHUT_RDWR)
    s.close()

if __name__ == '__main__':

    #print startListener(verbose = 1)
    pass
